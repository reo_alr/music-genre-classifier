import keras
import os
from keras.models import Sequential,Input,Model
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import LeakyReLU

import numpy as np
import matplotlib.pyplot as plt
import librosa

def one_hot_out(cat, limit):
	res = np.zeros((limit,))
	res[cat] = 1
	return res

# using 20 of each category as training data set and using 10 of each category as test data set
train_data_each_category = 50
test_data_each_category = 5
categories = ["classical", "jazz", "rock"]
folder = "genres/"
path = ""
train_data = []
train_label = []
test_data = []
test_label = []

# train_count = 0
# test_count = 0
# for i in range(0, len(categories)):
# 	train_count = 0
# 	test_count = 0
# 	path = folder+categories[i]+"/"
# 	for filename in os.listdir(path):
# 		if train_count==train_data_each_category and test_count==test_data_each_category:
# 			break
# 		y, sr = librosa.load(path+filename)
# 		mfccFeat = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=12)
# 		if train_count<train_data_each_category:
# 			train_data.append(mfccFeat)
# 			# np.append(train_data, mfccFeat)
# 			train_label.append(one_hot_out(i, len(categories)))
# 			# np.append(train_label, one_hot_out(i, len(categories)))
# 			train_count+=1
# 		elif test_count<test_data_each_category:
# 			test_data.append(mfccFeat)
# 			# np.append(test_data, mfccFeat)
# 			test_label.append(one_hot_out(i, len(categories)))
# 			# np.append(test_label, one_hot_out(i, len(categories)))
# 			test_count+=1

# print("Input successfully processed")

# td = np.empty((train_data_each_category*3, 12, 1293))
# i = 0
# for data in train_data:
# 	if(data.shape!=(12, 1293)):
# 		newdata = data
# 		newdata.resize(12, 1293, refcheck=False)
# 		td[i] = newdata
# 	else:
# 		td[i] = data
# 	i+=1
# train_data = td
# # train_data2 = np.array(train_data)
# # train_data = train_data2
# train_data = np.expand_dims(train_data, 3)
# train_label = np.array(train_label)

# td = np.empty((test_data_each_category*3, 12, 1293))
# i = 0
# for data in test_data:
# 	if(data.shape!=(12, 1293)):
# 		newdata = data
# 		newdata.resize(12, 1293, refcheck=False)
# 		td[i] = newdata
# 	else:
# 		td[i] = data
# 	i+=1
# test_data = td

# test_data = np.array(test_data)
# test_data = np.expand_dims(test_data, 3)
# test_label = np.array(test_label)
# print(train_data.shape)
# print(test_data.shape)

# np.save("train_file", train_data)
# np.save("train_label_file", train_label)
# np.save("test_file", test_data)
# np.save("test_label_file", test_label)

test_data = np.load("test_file.npy")
test_label = np.load("test_label_file.npy")

network_model = Sequential()
network_model.add(Conv2D(3, kernel_size=(12, 10), activation='linear', input_shape=(12, 1293, 1), padding='same'))
network_model.add(LeakyReLU(alpha=0.1))
network_model.add(Conv2D(15, kernel_size=(1, 10), activation='linear', padding='same'))
network_model.add(LeakyReLU(alpha=0.1))
network_model.add(Conv2D(65, kernel_size=(1, 1), activation='linear', padding='same'))
network_model.add(LeakyReLU(alpha=0.1))
network_model.add(Flatten())
network_model.add(Dense(len(categories), activation='linear'))
network_model.add(LeakyReLU(alpha=0.1))
network_model.compile(loss=keras.losses.categorical_crossentropy, optimizer=keras.optimizers.Adam(),metrics=['accuracy'])

# network_model.summary()

# # train
# epoch = 30
# network_training = network_model.fit(train_data, train_label, batch_size=150, epochs=epoch, verbose=1)

# # save model
# model_json = network_model.to_json()
# with open("model.json", "w") as json_file:
#     json_file.write(model_json)
# # save weight
# network_model.save_weights("model.h5")
# print("Saved model")

network_model.load_weights("model.h5")

predClass = network_model.predict(test_data)
predClass = np.argmax(np.round(predClass), axis=1)
for x in range(0, 15):
	i = x+1
	print("Test ", x, ": Predicted Class: ", categories[predClass[x]], " Actual: ", categories[np.argmax(test_label[x])])

# test = network_model.evaluate(test_data, test_label, verbose=1)
# print("Loss: ", test[0])
# print("Accuracy: ", test[1])